package nl.lolmewn.stats.command;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public class StatsResetOther extends StatsSubCommand {

    private final HashSet<String> confirm = new HashSet<String>();

    public StatsResetOther(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        Player find = getPlugin().getServer().getPlayer(args[0]);
        if (find == null) {
            sender.sendMessage(ChatColor.RED + "I'm sorry, but that player is offline!");
            return true;
        }
        if (!this.confirm.contains(sender.getName())) {
            this.confirm.add(sender.getName());
            sender.sendMessage(ChatColor.BLUE + "Please confirm you really want to do this by performing the command again");
            final String name = sender.getName();
            getPlugin().getServer().getScheduler().runTaskLater(getPlugin(), new Runnable() {
                @Override
                public void run() {
                    if (confirm.contains(name)) {
                        confirm.remove(name);
                        Player p = getPlugin().getServer().getPlayerExact(name);
                        if (p != null) {
                            p.sendMessage(ChatColor.RED + "Stats reset command expired.");
                        }
                    }
                }
            }, 200L);
            return true;
        }
        StatsPlayer player = getPlugin().getPlayerManager().hasPlayer(find) ? getPlugin().getPlayerManager().getPlayer(find) : null;
        if (player == null) {
            sender.sendMessage("Sorry, but this player doesn't seem to have any stats.");
            return true;
        }
        for (String world : player.getWorlds()) {
            for (StatData statData : player.getStatsForWorld(world)) {
                if (statData.getStat().getDataType().equals(StatDataType.FIXED)) {
                    continue; //silly 
                }
                for (Object[] vars : statData.getAllVariables()) {
                    if (statData.getStat().equals(getPlugin().getStatTypes().get("Lastjoin"))) {
                        statData.setCurrentValue(vars, System.currentTimeMillis());
                    } else {
                        statData.setCurrentValue(vars, 0);
                    }
                    statData.forceUpdate(vars);
                }
            }
        }

        Connection con = getPlugin().getMySQL().getConnection();
        for (StatsTable table : getPlugin().getStatsTableManager().values()) {
            if(table.getName().equals(getPlugin().getSettings().getDbPrefix() + "players")){
                continue;
            }
            if (!table.hasColumn("player_id")) {
                continue;
            }
            try {
                PreparedStatement st = con.prepareStatement("DELETE FROM " + table.getName() + " WHERE player_id=?");
                st.setInt(1, player.getId());
                st.executeUpdate();
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        player.setHasPlayerDatabaseRow(false);
        sender.sendMessage(ChatColor.GREEN + find.getName() + " has been reset!");
        this.confirm.remove(sender.getName());
        return true;
    }

}
