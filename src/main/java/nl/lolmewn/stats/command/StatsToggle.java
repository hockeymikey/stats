package nl.lolmewn.stats.command;

import nl.lolmewn.stats.Main;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public class StatsToggle extends StatsSubCommand {

    public StatsToggle(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("USAGE: /stats toggle <debug|query>");
            return true;
        }
        if (args[0].equals("debug")) {
            getPlugin().getSettings().setDebugging(!getPlugin().getSettings().isDebugging());
            sender.sendMessage("Debug value set to: " + getPlugin().getSettings().isDebugging());
            return true;
        }
        if (args[0].equals("query")) {
            getPlugin().query = !getPlugin().query;
            sender.sendMessage("Query debug value set to: " + getPlugin().query);
            return true;
        }
        return true;
    }

}
