package nl.lolmewn.stats.command;

import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.MySQL;
import nl.lolmewn.stats.Settings;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public class StatsReload extends StatsSubCommand {

    public StatsReload(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.BLUE + "Sending all remaining stats...");
        getPlugin().runTableUpdates();
        sender.sendMessage(ChatColor.BLUE + "Reloading plugin...");
        Settings set = new Settings(getPlugin());
        set.loadSettings();
        getPlugin().setSettings(set);
        getPlugin().getMySQL().exit();
        getPlugin().setMysql(new MySQL(getPlugin(), set.getDbHost(), set.getDbPort(),
                set.getDbUser(), set.getDbPass(),
                set.getDbName(), set.getDbPrefix()));
        if (getPlugin().getMySQL().isFault()) {
            getPlugin().getLogger().severe("MySQL connection failed, disabling plugin!");
            sender.sendMessage(ChatColor.RED + "MySQL connection failed, disabling plugin!");
            getPlugin().getServer().getPluginManager().disablePlugin(getPlugin());
            return true;
        }
        sender.sendMessage(ChatColor.GREEN + "Plugin reloaded succesfully!");
        return true;
    }

}
