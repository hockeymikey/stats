package nl.lolmewn.stats.command;

import java.util.Arrays;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public class StatsAdd extends StatsSubCommand {

    public StatsAdd(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length < 2) {
            sender.sendMessage("Correct usage: /stats add <statName> <playerName> <amount> [parameters]");
            sender.sendMessage("You can leave out <playerName> if you want to add it to yourself");
            return true;
        }
        String statName = args[0];
        Stat stat = getPlugin().getAPI().getStat(statName);
        if (stat == null) {
            sender.sendMessage("Stat not found!");
            return true;
        }
        StatsPlayer player;
        int amount;
        boolean self = false;
        if (args.length == 2) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You need to be a player to do this!");
                return true;
            }
            player = getPlugin().getPlayerManager().getPlayer((Player) sender);
            amount = Integer.parseInt(args[1]);
            self = true;
        } else {
            try {
                amount = Integer.parseInt(args[1]);
                if (!(sender instanceof Player)) {
                    sender.sendMessage("You need to be a player to do this!");
                    return true;
                }
                player = getPlugin().getPlayerManager().getPlayer((Player)sender);
                self = true;
            } catch (NumberFormatException e) {
                OfflinePlayer op = this.getPlugin().getServer().getOfflinePlayer(args[1]);
                if (getPlugin().getPlayerManager().hasPlayer(op)) {
                    player = getPlugin().getPlayerManager().getPlayer(op);
                    amount = Integer.parseInt(args[2]);
                } else {
                    sender.sendMessage("Correct usage: /stats add <statName> <playerName> <amount> [parameters]");
                    sender.sendMessage("You can leave out <playerName> if you want to add it to yourself");
                    sender.sendMessage("Either the command was used incorrectly or the player requested is offline");
                    return true;
                }
            }
        }
        Player bplayer = getPlugin().getServer().getPlayerExact(player.getPlayername());
        StatData data = player.getStatData(stat, bplayer.getWorld().getName(), true);
        String[] vars = Arrays.copyOfRange(args, self ? 2 : 3, args.length);
        data.addUpdate(vars, amount);
        bplayer.sendMessage("Added " + amount + " to " + stat.getName() + " for " + player.getPlayername() + " using vars " + Arrays.toString(vars));
        return true;
    }

}
