/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.api;

import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsPlayerLoadedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private final StatsPlayer player;

    public StatsPlayerLoadedEvent(StatsPlayer player, boolean async) {
        super(async);
        this.player = player;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public StatsPlayer getStatsPlayer() {
        return player;
    }

}
