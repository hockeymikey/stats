package nl.lolmewn.stats.api.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Lolmewn
 */
public class StatsColumn {

    private final StatsTable parentTable;
    private final String name;
    private final MySQLType type;
    private List<MySQLAttribute> attributes;
    private String def;

    public StatsColumn(StatsTable table, String name, MySQLType type) {
        this.parentTable = table;
        this.name = name;
        this.type = type;
    }

    public StatsColumn addAttributes(MySQLAttribute... attributes) {
        this.attributes = Arrays.asList(attributes);
        return this;
    }

    public boolean hasAttributes() {
        return this.attributes != null;
    }

    public List<MySQLAttribute> getAttributes() {
        return this.attributes;
    }

    public String getDefault() {
        return def;
    }

    public StatsColumn setDefault(String def) {
        this.def = def;
        return this;
    }

    public boolean hasDefault() {
        return def != null;
    }

    public String getName() {
        return name;
    }

    public MySQLType getType() {
        return type;
    }

    public StatsColumn addColumn(String name, MySQLType type) {
        return this.parentTable.addColumn(name, type);
    }

    public void dbIncrement(Connection con, List<StatsColumn> whereColumns, List<Object> whereValues, double amount) throws SQLException {
        if (whereColumns.size() != whereValues.size()) {
            System.out.println("[Stats][API]Can't increment column " + name + ", amount of where-columns isn't equal to amount of where-values");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(this.parentTable.getName()).append(" SET ").append(this.name);
        sb.append("=").append(this.name).append("+? ");
        if (whereColumns.size() > 0) {
            for (int i = 0; i < whereColumns.size(); i++) {
                StatsColumn where = whereColumns.get(i);
                if (i == 0) {
                    sb.append("WHERE ");
                } else {
                    sb.append(" AND ");
                }
                sb.append(where.getName()).append("=?");
            }
        }
        PreparedStatement st = con.prepareStatement(sb.toString());
        st.setDouble(1, amount);
        for (int i = 0; i < whereValues.size(); i++) {
            Object value = whereValues.get(i);
            st.setObject(i + 2, value);
        }
        st.executeUpdate();
    }

    public void dbSet(Connection con, List<StatsColumn> whereColumns, List<Object> whereValues, Object object) throws SQLException {
        if (whereColumns == null) {
            if (whereValues != null) {
                System.out.println("[Stats][API]Can't increment column " + name + ", amount of where-columns isn't equal to amount of where-values");
                return;
            }
        }
        if (whereValues == null) {
            if (whereColumns != null) {
                System.out.println("[Stats][API]Can't increment column " + name + ", amount of where-columns isn't equal to amount of where-values");
                return;
            }
        }
        if (whereColumns != null && whereValues != null && whereColumns.size() != whereValues.size()) {
            System.out.println("[Stats][API]Can't increment column " + name + ", amount of where-columns isn't equal to amount of where-values");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(this.parentTable.getName()).append(" SET ").append(this.name);
        sb.append("=?");
        if (whereColumns != null && whereColumns.size() > 0) {
            for (int i = 0; i < whereColumns.size(); i++) {
                StatsColumn where = whereColumns.get(i);
                if (i == 0) {
                    sb.append("WHERE ");
                } else {
                    sb.append(" AND ");
                }
                sb.append(where.getName()).append("=?");
            }
        }
        PreparedStatement st = con.prepareStatement(sb.toString());
        st.setObject(1, object);
        if (whereValues != null) {
            for (int i = 0; i < whereValues.size(); i++) {
                Object value = whereValues.get(i);
                st.setObject(i + 2, value);
            }
        }
        st.executeUpdate();
    }

}
