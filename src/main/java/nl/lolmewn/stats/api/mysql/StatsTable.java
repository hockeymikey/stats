package nl.lolmewn.stats.api.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import static nl.lolmewn.stats.api.mysql.MySQLAttribute.AUTO_INCREMENT;
import static nl.lolmewn.stats.api.mysql.MySQLAttribute.NOT_NULL;
import static nl.lolmewn.stats.api.mysql.MySQLAttribute.PRIMARY_KEY;
import static nl.lolmewn.stats.api.mysql.MySQLType.BOOLEAN;
import static nl.lolmewn.stats.api.mysql.MySQLType.DOUBLE;
import static nl.lolmewn.stats.api.mysql.MySQLType.INTEGER;
import static nl.lolmewn.stats.api.mysql.MySQLType.STRING;
import static nl.lolmewn.stats.api.mysql.MySQLType.TIMESTAMP;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.Bukkit;

/**
 *
 * @author Lolmewn
 */
public class StatsTable {

    private final String tableName;
    private final transient LinkedHashMap<String, Stat> stats = new LinkedHashMap<String, Stat>(); //StatName - Stat
    private final transient LinkedHashMap<String, StatsColumn> columns = new LinkedHashMap<String, StatsColumn>(); //ColumnName (equal to StatName if it exists) - StatsColumn
    //private final transient LinkedHashMap<String, List<MySQLAttribute>> attributes = new LinkedHashMap<String, List<MySQLAttribute>>(); //ColumnName (equal to StatName if it exists) - List of attributes
    //private final transient HashMap<String, String> defaults = new HashMap<String, String>();
    private final boolean usingSnapshots;

    /**
     * Creates a StatsTable object.
     *
     * @param name The name of the Table. For example: Stats_player The database
     * prefix MUST already be in the name.
     * @param addDefaultColumns adds the counter and player_id columns. Just for
     * your convenience!
     * @param usingSnapshots whether or not the table should have columns
     * "snapshot_name" and "snapshot_time".
     */
    public StatsTable(String name, boolean addDefaultColumns, boolean usingSnapshots) {
        this.tableName = name;
        if (addDefaultColumns) {
            this.addColumn("counter", INTEGER).addAttributes(AUTO_INCREMENT, NOT_NULL, PRIMARY_KEY);
            this.addColumn("player_id", INTEGER).addAttributes(NOT_NULL);
            this.addColumn("world", STRING).addAttributes(NOT_NULL).setDefault("'" + Bukkit.getServer().getWorlds().get(0).getName() + "'");
        }
        if (usingSnapshots) {
            this.addColumn("snapshot_name", STRING).addAttributes(MySQLAttribute.NOT_NULL).setDefault("'main_snapshot'");
            this.addColumn("snapshot_time", TIMESTAMP).addAttributes(MySQLAttribute.NOT_NULL).setDefault("NOW()");
        }
        this.usingSnapshots = usingSnapshots;
    }

    /**
     * Adds a stat to this table, without a default value
     *
     * @param stat Stat to add
     */
    public final void addStat(Stat stat) {
        addStat(stat, null);
    }

    /**
     * Adds a stat to this table, with a default value
     *
     * @param stat Stat to add
     * @param def Default value string. Note that this must follow MySQL markup
     * , so if it's a string you want to make default, use 'default' instead
     * (with the quotes!)
     */
    public final void addStat(Stat stat, String def) {
        this.stats.put(stat.getName(), stat);
        if (stat.getTableType().equals(StatTableType.COLUMN)) {
            this.addColumn(stat.getValueColumn() != null
                    ? stat.getValueColumn()
                    : stat.getName().toLowerCase().replace(" ", ""), stat.getMySQLType()).setDefault(def);
        }
    }

    /**
     * Adds a column to this table.
     *
     * @param name Name of the column, also the name in the database
     * @param dataType What kind of column it is (INT/DOUBLE/etc)
     * @return Newly created StatsColumn object
     */
    public final StatsColumn addColumn(String name, MySQLType dataType) {
        StatsColumn column = new StatsColumn(this, name, dataType);
        this.columns.put(name, column);
        return column;

    }

    @Deprecated
    /**
     * Deprecated, add the attributes to the column instead when creating the
     * column, like statsTableObject.addColumn(name,
     * type).addAttributes(attribute1, attribute2...)
     */
    public final void addAttributes(String column, List<MySQLAttribute> attributes) {
        if (!this.columns.containsKey(column)) {
            return;
        }
        for (MySQLAttribute att : attributes) {
            this.columns.get(column).addAttributes(att);
        }
    }

    @Deprecated
    /**
     * Deprecated, add the attributes to the column instead when creating the
     * column, like statsTableObject.addColumn(name, type).setDefault(default);
     */
    public final void addDefaults(String column, String defaultValue) {
        if (!this.columns.containsKey(column)) {
            return;
        }
        this.columns.get(column).setDefault(defaultValue);
    }

    public String generateCreateTable() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(tableName).append("(");
        Iterator<String> it = columns.keySet().iterator();
        while (it.hasNext()) {
            String columnName = it.next();
            StatsColumn column = columns.get(columnName);
            sb.append(column.getName()).append(" ").append(column.getType().getMySQLEquiv()).append(" ");
            if (column.hasAttributes()) {
                for (MySQLAttribute ma : column.getAttributes()) {
                    sb.append(ma.getMySQLEquiv()).append(" ");
                }
            }
            if (column.hasDefault()) {
                sb.append("DEFAULT ").append(column.getDefault()).append(" ");
            }
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * Name of the table. Prefix included.
     *
     * @return tableName
     */
    public String getName() {
        return tableName;
    }

    /**
     * Get all stats that have been added using .addStat()
     *
     * @return Collection of Stat objects
     */
    public Collection<Stat> getAssociatedStats() {
        return this.stats.values();
    }

    /**
     * Get all the column names for this table
     *
     * @return names of all columns
     */
    public Collection<String> getColumnNames() {
        return this.columns.keySet();
    }

    /**
     * Get all StatsColumn objects from this table
     *
     * @return objects of all StatsColumns
     */
    public Collection<StatsColumn> getColumns() {
        return this.columns.values();
    }

    public StatsColumn getColumn(String name) {
        return this.columns.get(name);
    }

    public void validateColumns(Connection con) throws SQLException {
        ResultSet set = con.createStatement().executeQuery("SELECT * FROM " + this.tableName);
        ResultSetMetaData rsmd = set.getMetaData();
        for (String columnName : this.getColumnNames()) {
            boolean cont = false;
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                if (rsmd.getColumnName(i).equalsIgnoreCase(columnName)) {
                    cont = true;
                    break;
                }
            }
            if (cont) {
                continue;
            }
            StatsColumn column = this.columns.get(columnName);
            System.out.println("[Stats] Found a column that doesn't exist yet in table " + this.tableName + "; adding it: " + column.getName());
            Statement st = con.createStatement();
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append(this.tableName).append(" ADD COLUMN ").append(columnName).append(" ").append(column.getType().getMySQLEquiv()).append(" ");
            if (column.hasAttributes()) {
                for (MySQLAttribute ma : column.getAttributes()) {
                    sb.append(ma.getMySQLEquiv()).append(" ");
                }
            }
            if (column.hasDefault()) {
                sb.append("DEFAULT ").append(column.getDefault());
            }
            st.executeUpdate(sb.toString());

        }
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            boolean cont = false;
            for (String columnName : this.getColumnNames()) {
                if (rsmd.getColumnName(i).equalsIgnoreCase(columnName)) {
                    cont = true;
                    break;
                }
            }
            if (cont) {
                continue;
            }
            System.out.println("[Stats] Found a column in table " + this.tableName + " that is not used: " + rsmd.getColumnName(i));
        }
        Statement st = con.createStatement();
        if (st.executeQuery("SHOW INDEXES FROM " + this.tableName + " WHERE Key_name='no_duplicates'").next()) {
            st.execute("DROP INDEX no_duplicates ON " + this.tableName);
        }
        st.close();
    }

    public boolean hasColumn(String name) {
        return this.columns.containsKey(name);
    }

    public void updateStat(Connection con, Stat stat, StatsPlayer player) throws SQLException {
        if (stat.getDataSaver() != null) {
            stat.getDataSaver().save(player, stat, con);
            return;
        }
        if (stat.getValueColumn() == null) {
            throw new NullPointerException("Both saver and value are null, impossible");
        }
        if (!this.hasColumn("player_id")) {
            throw new IllegalStateException("Table doesn't have player_id column - can't save");
        }
        boolean hasWorldColumn = hasColumn("world");
        StringBuilder update = new StringBuilder();
        update.append("UPDATE ").append(this.tableName);
        update.append(" SET ").append(stat.getValueColumn()).append("=");
        if (stat.getDataType().equals(StatDataType.INCREASING)) {
            update.append("IFNULL(").append(stat.getValueColumn()).append(", 0)").append("+");
        }
        update.append("? WHERE player_id=? ");
        if (hasWorldColumn) {
            update.append("AND world=? ");
        }
        if (stat.getVarColumns().length != 0 || this.usingSnapshots) {
            update.append("AND ");
        }
        for (int i = 0; i < stat.getVarColumns().length; i++) {
            String column = stat.getVarColumns()[i];
            if (!hasColumn(column)) {
                System.out.println("Unknown column " + column + " in table " + this.tableName + ", trying update query anyway (I hope you know what you're doing!)");
            }
            update.append(column).append("=? ");
            if (i < stat.getVarColumns().length - 1 || this.usingSnapshots) {
                update.append("AND ");
            }
        }
        if (this.usingSnapshots) {
            update.append("snapshot_name=?");
        }

        PreparedStatement updateStatement = con.prepareStatement(update.toString());
        updateStatement.setInt(2, player.getId());
        if (this.usingSnapshots) {
            updateStatement.setString(3 + stat.getVarColumns().length + (hasWorldColumn ? 1 : 0), "main_snapshot");
        }
        for (String world : player.getWorlds()) {
            if (hasWorldColumn) {
                updateStatement.setString(3, world);
            }
            StatData data = player.getStatData(stat, world, false);
            if (data == null) {
                continue;
            }
            for (Object[] vars : data.getUpdateVariables()) {
                if (vars.length != stat.getVarColumns().length) {
                    throw new IllegalStateException("Variable length does not match the variable column length - can't update");
                }
                double value = stat.getDataType().equals(StatDataType.INCREASING) ? data.getUpdateValue(vars, true) : data.getValue(vars, true);
                if (stat.getMySQLType().equals(MySQLType.TIMESTAMP)) {
                    updateStatement.setTimestamp(1, new Timestamp((long) value));
                } else {
                    updateStatement.setDouble(1, value);
                }
                for (int i = 0; i < vars.length; i++) {
                    Object obj = vars[i];
                    updateStatement.setObject(3 + i + (hasWorldColumn ? 1 : 0), obj);
                }
                int succes = updateStatement.executeUpdate();
                if (succes == 0) {
                    insertStat(con, stat.getVarColumns(), vars, player, stat, world, value);
                }
            }
        }
        updateStatement.close();
    }

    public void insertStat(Connection con, String[] variableColumns, Object[] variableValues, StatsPlayer player, Stat stat, String world, double value) throws SQLException {
        if (variableColumns.length != variableValues.length) {
            throw new IllegalStateException("Column amount does not equal values amount - can't insert");
        }
        StringBuilder insert = new StringBuilder();
        insert.append("INSERT INTO ").append(this.tableName).append("(player_id");  //player_id
        if (hasColumn("world")) {                                                   //world
            insert.append(", world");
        }
        if (this.usingSnapshots) {                                                  //snapshot_name, snapshot_time
            insert.append(", snapshot_name, snapshot_time");
        }
        for (String column : variableColumns) {                                     //variables
            if (!hasColumn(column)) {
                System.out.println("Unknown column " + column + " in table " + this.tableName + ", trying insert query anyway (I hope you know what you're doing!)");
            }
            insert.append(", ").append(column);
        }
        insert.append(", ").append(stat.getValueColumn());                          //value
        insert.append(") VALUES (?");
        if (hasColumn("world")) {
            insert.append(", ?");
        }
        if (this.usingSnapshots) {
            insert.append(", ?, ?");
        }
        for (int i = 0; i < variableColumns.length; i++) {
            insert.append(", ?");
        }
        insert.append(", ?");
        insert.append(")");

        PreparedStatement insertStatement = con.prepareStatement(insert.toString());
        int i = 1;
        insertStatement.setInt(i++, player.getId());
        if (hasColumn("world")) {
            insertStatement.setString(i++, world);
        }
        if (this.usingSnapshots) {
            insertStatement.setString(i++, "main_snapshot");
            insertStatement.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
        }
        for (Object var : variableValues) {
            insertStatement.setObject(i++, var);
        }
        if (stat.getMySQLType().equals(MySQLType.TIMESTAMP)) {
            insertStatement.setTimestamp(i++, new Timestamp((long) value));
        } else {
            insertStatement.setDouble(i++, value);
        }
        insertStatement.execute();
    }

    public void loadStats(Connection con, StatsPlayer player) throws SQLException {
        for (Stat stat : this.getAssociatedStats()) {
            if (stat.getDataLoader() != null) {
                PreparedStatement st = con.prepareStatement("SELECT * FROM " + this.tableName + " WHERE player_id=?" + (this.usingSnapshots ? " AND snapshot_name=?" : ""));
                st.setInt(1, player.getId());
                if (this.usingSnapshots) {
                    st.setString(2, "main_snapshot");
                }
                ResultSet set = st.executeQuery();
                while (set.next()) {
                    for (Stat allStats : this.getAssociatedStats()) {
                        if (allStats.getDataLoader() == null) {
                            this.loadStat(con, player, allStats);
                        } else {
                            stat.getDataLoader().load(player, allStats, set);
                        }
                    }
                }
                return;
            } else {
                this.loadStat(con, player, stat);
            }
        }
    }

    public void loadStat(Connection con, StatsPlayer player, Stat stat) throws SQLException {
        StringBuilder select = new StringBuilder();
        select.append("SELECT ");
        select.append(stat.getValueColumn());
        boolean hasWorld = this.hasColumn("world");
        if (hasWorld) {
            select.append(",world");
        }
        for (int i = 0; i < stat.getVarColumns().length; i++) {
            String col = stat.getVarColumns()[i];
            select.append(",");
            select.append(col);
        }
        select.append(" FROM ").append(this.tableName).append(" WHERE player_id=?");
        if (this.usingSnapshots) {
            select.append(" AND snapshot_name=?");
        }
        PreparedStatement st = con.prepareStatement(select.toString());
        st.setInt(1, player.getId());
        if (this.usingSnapshots) {
            st.setString(2, "main_snapshot");
        }
        ResultSet set = st.executeQuery();
        while (set.next()) {
            Object[] vars = new Object[stat.getVarColumns().length];
            int i = 0;
            for (String col : stat.getVarColumns()) {
                vars[i++] = set.getObject(col);
            }
            StatData sd = player.getStatData(stat, hasWorld ? set.getString("world") : "__GLOBAL__", true);
            switch (stat.getMySQLType()) {
                case BOOLEAN:
                    sd.setCurrentValue(vars, set.getBoolean(stat.getValueColumn()) == true ? 1 : 0);
                    break;
                case DOUBLE:
                case INTEGER:
                    sd.setCurrentValue(vars, set.getDouble(stat.getValueColumn()));
                    break;
                case TIMESTAMP:
                    Timestamp stamp = set.getTimestamp(stat.getValueColumn());
                    if (stamp != null) {
                        sd.setCurrentValue(vars, stamp.getTime());
                    }
            }
        }
    }

    public void dropColumn(Connection con, String name) throws SQLException {
        if (!this.hasColumn(name)) {
            return;
        }
        this.columns.remove(name);

        StringBuilder sb = new StringBuilder();
        sb.append("ALTER TABLE ").append(this.tableName).append(" DROP ").append(name);
        Statement st = con.createStatement();
        st.execute(sb.toString());
        st.close();
    }
}
