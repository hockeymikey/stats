package nl.lolmewn.stats.api.mysql;

/**
 *
 * @author Lolmewn
 */
public enum MySQLType {

    STRING("VARCHAR(255)"),
    TIMESTAMP("TIMESTAMP"),
    BOOLEAN("BOOLEAN"),
    DOUBLE("DOUBLE"),
    FLOAT("FLOAT"),
    INTEGER("INT"),
    BYTE_ARRAY("BLOB");

    private final String mysqlEquiv;

    private MySQLType(String mysqlEquiv) {
        this.mysqlEquiv = mysqlEquiv;
    }

    public String getMySQLEquiv() {
        return this.mysqlEquiv;
    }

}
