package nl.lolmewn.stats.api.mysql;

/**
 *
 * @author Lolmewn
 */
public enum StatTableType {

    COLUMN, TABLE;

}
