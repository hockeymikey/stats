package nl.lolmewn.stats.loader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class FirstjoinLoader extends DataLoader {

    public FirstjoinLoader(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean load(StatsPlayer player, Stat stat, ResultSet set) throws SQLException {
        if (set == null) {
            System.out.println("ResultSet is null, printing database layout");
            Connection con = this.getAPI().getConnection();
            Statement st = con.createStatement();
            ResultSet resSet = st.executeQuery("DESCRIBE " + this.getAPI().getDatabasePrefix() + "players");
            if (resSet == null) {
                System.out.println("Looks like the table doesn't exist or there's something wrong with the connection.");
                System.out.println("Please manually check your database to see if the " + this.getAPI().getDatabasePrefix() + "players table exists");
                return false;
            }
            while (resSet.next()) {
                System.out.println("Column: " + resSet.getString(1));
            }
            return false;
        }
        if (set.getTimestamp("firstjoin") != null) {
            player.getStatData(stat, "__GLOBAL__", true).setCurrentValue(new Object[]{}, set.getTimestamp("firstjoin").getTime());
        }
        return true;
    }

}
