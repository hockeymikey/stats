package nl.lolmewn.stats;

import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.api.saver.DataSaver;

/**
 *
 * @author Lolmewn
 */
public class DefaultStat extends Stat {

    public DefaultStat(String name, StatDataType dataType, StatTableType tableType, StatsTable table, MySQLType type, DataLoader dataLoader, DataSaver dataSaver) {
        super(name, dataType, tableType, table, type, dataLoader, dataSaver);
    }

    public DefaultStat(String name, StatDataType dataType, StatTableType tableType, StatsTable table, MySQLType type, String valueColumn, String[] varColumns) {
        super(name, dataType, tableType, table, type, valueColumn, varColumns);
    }

    public GlobalDataHolder getGlobalStatsHolder() {
        return null; //TODO
    }

}
