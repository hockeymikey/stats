/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.player;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatUpdateEvent;
import nl.lolmewn.stats.signs.StatsSign;
import org.bukkit.Bukkit;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatData {

    private Stat type;
    private StatsPlayer superClass;
    private ConcurrentHashMap<String, Double> rows;
    private ConcurrentHashMap<String, Double> currentValues; //in database, that is
    private final Set<String> hasUpdate = Collections.synchronizedSet(new HashSet<String>());
    private final ConcurrentHashMap<String, Object[]> linker = new ConcurrentHashMap<String, Object[]>();
    private final Set<String> signs = Collections.synchronizedSet(new HashSet<String>());

    public StatData(StatsPlayer superClass, Stat type, boolean usingDouble) {
        this(superClass, type);
    }

    public StatData(StatsPlayer superClass, Stat type) {
        this.superClass = superClass;
        this.type = type;
        rows = new ConcurrentHashMap<String, Double>();
        currentValues = new ConcurrentHashMap<String, Double>();
    }

    public Stat getStat() {
        return type;
    }

    public StatsPlayer getStatsPlayer() {
        return superClass;
    }

    public boolean hasUpdate(Object[] variables) {
        return hasUpdate.contains(Arrays.toString(variables));
    }

    @Deprecated
    public double getUpdateValueDouble(Object[] variables, boolean updatingDatabase) {
        return this.getUpdateValue(variables, updatingDatabase);
    }

    public double getUpdateValue(Object[] variables, boolean updatingDatabase) {
        String hash = Arrays.toString(variables);
        if (!this.currentValues.containsKey(hash)) {
            if (updatingDatabase) {
                this.currentValues.put(hash, this.rows.get(hash));
                this.hasUpdate.remove(hash);
            }
            return this.rows.containsKey(hash) ? this.rows.get(hash) : 0;
        }
        double update = this.hasUpdate(variables) ? this.rows.get(hash) - this.currentValues.get(hash) : 0;
        if (updatingDatabase) {
            this.currentValues.put(hash, this.rows.get(hash));
            this.hasUpdate.remove(hash);
        }
        return update;
    }

    public void addUpdate(final Object[] variables, final double add) {
        final StatUpdateEvent event = new StatUpdateEvent(this, add, variables, true);
        final boolean temp = this.superClass.isTemp();
        Bukkit.getScheduler().runTaskAsynchronously(superClass.getPlugin(), new Runnable() {
            @Override
            public void run() {
                if (!temp) {
                    Bukkit.getPluginManager().callEvent(event);
                }
                if (event.isCancelled()) {
                    return;
                }
                String hash = Arrays.toString(variables);
                if (rows.containsKey(hash)) {
                    rows.put(hash, rows.get(hash) + event.getUpdateValue());
                } else {
                    rows.put(hash, event.getUpdateValue());
                    linker.put(hash, variables);
                }
                if (!hasUpdate(variables)) {
                    hasUpdate.add(hash);
                }
            }
        });
    }

    /**
     * [b]This method should ONLY be used for moving![/b]
     *
     * @param variables The variables this should add to (type=0 for example)
     * @param add - the amount to add to the stat
     */
    @Deprecated
    public void addUpdateDouble(final Object[] variables, final double add) {
        this.addUpdate(variables, add);
    }

    public void setCurrentValue(Object[] variables, double value) {
        String hash = Arrays.toString(variables);
        this.currentValues.put(hash, value);
        this.rows.put(hash, value);
        this.linker.put(hash, variables);
    }

    @Deprecated
    public void setCurrentValueDouble(Object[] variables, double value) {
        this.setCurrentValue(variables, value);
    }

    public HashSet<Object[]> getUpdateVariables() {
        HashSet<Object[]> withUpdates = new HashSet<Object[]>();
        for (String hash : this.hasUpdate) {
            withUpdates.add(this.linker.get(hash));
        }
        return withUpdates;
    }

    public Collection<Object[]> getAllVariables() {
        return linker.values();
    }

    public double getValue(Object[] vars, boolean updatingInDatabase) {
        if (updatingInDatabase) {
            this.hasUpdate.remove(Arrays.toString(vars));
        }
        return getValue(vars);
    }

    public double getValue(Object[] vars) {
        if (!rows.containsKey(Arrays.toString(vars))) {
            return 0;
        }
        return rows.get(Arrays.toString(vars));
    }

    @Deprecated
    public double getValueDouble(Object[] vars) {
        return this.getValue(vars);
    }

    /**
     * Gives the first value it can find. If it can't find anything, 0 will be
     * returned. Usecase is only when you have no clue how to use variables and
     * are too lazy to check em out ;)
     *
     * @return value of this StatData
     */
    public double getValueUnsafe() {
        if (this.rows != null && !this.rows.isEmpty()) {
            return rows.get(rows.keySet().iterator().next());
        }
        return 0;
    }

    public boolean hasValue(Object[] object) {
        if (rows != null) {
            return rows.containsKey(Arrays.toString(object));
        }
        return false;
    }

    public void forceUpdate(Object[] variables) {
        this.hasUpdate.add(Arrays.toString(variables));
    }

    public void removeHasUpdate(Object[] object) {
        this.hasUpdate.remove(Arrays.toString(object));
    }

    public void addSign(StatsSign sign) {
        this.signs.add(sign.getLocationString());
        sign.setAttachedToStat(true);
    }

    @Deprecated
    public void addSign(StatsSign sign, Object[] vars) {
        this.addSign(sign);
    }

    public Collection<String> getSignLocations() {
        return this.signs;
    }

    public boolean hasSigns() {
        return !signs.isEmpty();
    }

    public void removeSign(StatsSign sign) {
        this.signs.remove(sign.getLocationString());
    }

    public void debug() {
        Main p = superClass.getPlugin();
        p.debug("=====" + this.getStat().toString() + "=====");
        p.debug("Rows");
        for (String value : this.rows.keySet()) {
            p.debug("  " + value + ": " + this.rows.get(value));
        }
        p.debug("Current values");
        for (String value : this.currentValues.keySet()) {
            p.debug("  " + value + ": " + this.currentValues.get(value));
        }
        p.debug("Update arrays");
        for (String array : this.hasUpdate) {
            p.debug(array);
        }
    }
}
