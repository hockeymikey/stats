/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.player;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.signs.StatsSign;
import org.apache.commons.lang.Validate;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsPlayer {

    private final ConcurrentHashMap<String, ConcurrentHashMap<Stat, StatData>> worldStats;
    private final String playername;
    private boolean hasDBRow = false;
    private final Main plugin;
    private final boolean temp;

    private int id = -1;

    public StatsPlayer(Main m, String playername, boolean temp) {
        this.plugin = m;
        this.playername = playername;
        this.temp = temp;
        this.worldStats = new ConcurrentHashMap<String, ConcurrentHashMap<Stat, StatData>>();
    }

    /**
     * Get the id of this object
     * @return 
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name of the player to which this object belongs
     * @return 
     */
    public String getPlayername() {
        return playername;
    }

    protected Main getPlugin() {
        return plugin;
    }

    /**
     * Checks if a player has a stat in any world
     * @param stat Stat to check for
     * @return true if player has this Stat in at least one world, false otherwise
     */
    public boolean hasStat(Stat stat) {
        for(String world : this.worldStats.keySet()){
            if(hasStat(stat, world)){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if this player has a stat in the given world
     * @param stat Stat to check for
     * @param world World to check in
     * @return true if player has this Stat for that world, false otherwise
     */
    public boolean hasStat(Stat stat, String world) {
        Validate.notNull(stat);
        Validate.notNull(world);
        return this.worldStats.containsKey(world) && this.worldStats.get(world).containsKey(stat);
    }

    public void addStat(final Stat stat, final StatData statData, String world) {
        Validate.notNull(stat);
        Validate.notNull(statData);
        Validate.notNull(world);
        if (this.worldStats.containsKey(world)) {
            this.worldStats.get(world).put(stat, statData);
        } else {
            this.worldStats.put(world, new ConcurrentHashMap<Stat, StatData>() {
                {
                    this.put(stat, statData);
                }
            });
        }
    }

    /**
     * Get the {@link nl.lolmewn.stats.player.StatData} object the given world
     * and Stat.
     *
     * @param stat Stat to look up
     * @param world World to look it up for. If you want to look up the total of
     * all worlds, use {@link #getGlobalStatData(nl.lolmewn.stats.api.Stat)}
     * instead
     * @param createIfNotExists Whether or not to create and return a new
     * StatData object when one does currently not exist for this world
     * @return {@link nl.lolmewn.stats.player.StatData} object for this
     * Stat/world combination
     */
    public StatData getStatData(Stat stat, String world, boolean createIfNotExists) {
        Validate.notNull(stat);
        Validate.notNull(world);
        if (!hasStat(stat, world) && createIfNotExists) {
            this.addStat(stat, new StatData(this, stat), world);
            return this.getStatData(stat, world, false);
        }
        if (!this.worldStats.containsKey(world)) {
            return null;
        }
        return this.worldStats.get(world).get(stat);
    }

    /**
     * Get a combined {@link nl.lolmewn.stats.player.StatData} object made up of
     * all StatData objects for this Stat. This is the same as looping over
     * {@link #getWorlds()} and getting the StatData object for each those, then
     * combining them.
     *
     * @param stat Stat to look up
     * @return {@link nl.lolmewn.stats.player.StatData} object for this
     * Stat/world combination
     */
    public StatData getGlobalStatData(Stat stat) {
        Validate.notNull(stat);
        StatData data = new StatData(this, stat);
        for (String world : this.getWorlds()) {
            if (this.worldStats.get(world).containsKey(stat)) {
                StatData worldData = this.worldStats.get(world).get(stat);
                for (Object[] vars : worldData.getAllVariables()) {
                    data.setCurrentValue(vars, worldData.getValue(vars) + data.getValue(vars) + worldData.getUpdateValue(vars, false));
                }
            }
        }
        return data;
    }

    private Stat getStat(String name) {
        return this.plugin.getStatTypes().get(name);
    }

    public void updateStat(Stat stat, Object[] variables, double value, String world) {
        Validate.notNull(stat);
        Validate.notNull(world);
        this.getStatData(stat, world, true).addUpdate(variables, value);
    }

    public Iterable<StatData> getStatsForWorld(String world) {
        return this.worldStats.get(world).values();
    }

    public Iterable<String> getWorlds() {
        return this.worldStats.keySet();
    }

    public ConcurrentHashMap<String, ConcurrentHashMap<Stat, StatData>> getWorldStats() {
        return this.worldStats;
    }

    public boolean hasPlayerDatabaseRow() {
        return this.hasDBRow;
    }

    public void setHasPlayerDatabaseRow(boolean value) {
        this.hasDBRow = value;
    }

    /**
     * This method is used by Stats and should only be used by Stats. This
     * method syncs the data of one StatsPlayer with another. The data of the
     * StatsPlayer provided will be added to the data of this StatsPlayer
     * object. Prior to syncing, the thread sleeps for 50ms to allow for final
     * calculations by the other StatsPlayer object.
     *
     * @param get The StatsPlayer to get the data from
     */
    public void syncData(StatsPlayer get) {
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            Logger.getLogger(StatsPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getPlugin().debug("Sync initiated for " + this.getPlayername());
        for (String world : get.getWorlds()) {
            for (StatData statData : get.getStatsForWorld(world)) {
                this.getPlugin().debug("Syncing " + statData.getStat().toString());
                statData.debug();
                for (Object[] vars : statData.getUpdateVariables()) {
                    this.getPlugin().debug("Update vars: " + Arrays.toString(vars));
                    if (statData.getStat().equals(this.getStat("Lastjoin")) || statData.getStat().equals(this.getStat("Lastleave")) || statData.getStat().equals(this.getStat("Firstjoin"))) {
                        double syncValue = statData.getValue(vars);
                        this.getStatData(statData.getStat(), world, true).setCurrentValue(vars, syncValue);
                        this.getStatData(statData.getStat(), world, true).forceUpdate(vars);
                        continue;
                    }
                    double syncValue = statData.getValue(vars);
                    this.getStatData(statData.getStat(), world, true).addUpdate(vars, syncValue);
                }
            }
        }
    }

    public void addSignReference(StatsSign sign, String world) {
        Validate.notNull(sign);
        Validate.notNull(world);
        if (sign.getStat() == null) {
            this.getPlugin().getLogger().warning("Tried to assign sign on loc " + sign.getLocationString() + " to player " + this.getPlayername() + ", but Stat is null.");
            return;
        }
        this.getStatData(sign.getStat(), world, true).addSign(sign);
    }

    public boolean isTemp() {
        return temp;
    }

    protected void setId(int id) {
        this.id = id;
    }

    public void removeStat(Stat stat) {
        for (String world : this.getWorlds()) {
            this.removeStat(stat, world);
        }
    }

    public void removeStat(Stat stat, String world) {
        if (!this.hasStat(stat, world)) {
            return;
        }
        this.worldStats.get(world).remove(stat);
    }
}
