package nl.lolmewn.stats;

import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.loader.DataLoader;
import static nl.lolmewn.stats.api.mysql.MySQLAttribute.AUTO_INCREMENT;
import static nl.lolmewn.stats.api.mysql.MySQLAttribute.NOT_NULL;
import static nl.lolmewn.stats.api.mysql.MySQLAttribute.PRIMARY_KEY;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.api.saver.DataSaver;
import nl.lolmewn.stats.loader.BlockLoader;
import nl.lolmewn.stats.loader.FirstjoinLoader;
import nl.lolmewn.stats.saver.BlockSaver;
import nl.lolmewn.stats.saver.FirstjoinSaver;

/**
 *
 * @author Lolmewn
 */
public class DefaultStatsAndTables {

    private Main plugin;

    public void loadDefaults(Main main, StatsAPI api) {
        this.plugin = main;
        createPlayerTableStats(api);
        createBlockTableStats(api);
        createMoveTableStats(api);
        createKillTableStats(api);
        createDeathTableStats(api);
        createPVPTableStats(api);
        createPlayersTable(api);
        createSystemTable(api);
    }

    private void createPlayerTableStats(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "player";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addStat(addStat("Playtime", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "playtime", null), "0");
        table.addStat(addStat("Arrows", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "arrows", null), "0");
        table.addStat(addStat("Xp gained", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "xpgained", null), "0");
        table.addStat(addStat("Joins", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "joins", null), "0");
        table.addStat(addStat("Fish catched", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "fishcatched", null), "0");
        table.addStat(addStat("Damage taken", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "damagetaken", null), "0");
        table.addStat(addStat("Times kicked", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "timeskicked", null), "0");
        table.addStat(addStat("Tools broken", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "toolsbroken", null), "0");
        table.addStat(addStat("Eggs thrown", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "eggsthrown", null), "0");
        table.addStat(addStat("Items crafted", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "itemscrafted", null), "0");
        table.addStat(addStat("Omnomnom", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "omnomnom", null), "0");
        table.addStat(addStat("On fire", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "onfire", null), "0");
        table.addStat(addStat("Words said", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "wordssaid", null), "0");
        table.addStat(addStat("Commands done", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "commandsdone", null), "0");
        table.addStat(addStat("Lastleave", StatDataType.FIXED, StatTableType.COLUMN, table, MySQLType.TIMESTAMP, "lastleave", null), "0");
        table.addStat(addStat("Lastjoin", StatDataType.FIXED, StatTableType.COLUMN, table, MySQLType.TIMESTAMP, "lastjoin", null), "0");
        table.addStat(addStat("Votes", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "votes", null), "0");
        table.addStat(addStat("Teleports", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "teleports", null), "0");
        table.addStat(addStat("Itempickups", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "itempickups", null), "0");
        table.addStat(addStat("Bedenter", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "bedenter", null), "0");
        table.addStat(addStat("Bucketfill", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "bucketfill", null), "0");
        table.addStat(addStat("Bucketempty", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "bucketempty", null), "0");
        table.addStat(addStat("Worldchange", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "worldchange", null), "0");
        table.addStat(addStat("Itemdrops", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "itemdrops", null), "0");
        table.addStat(addStat("Shear", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "shear", null), "0");
        table.addStat(addStat("PVPStreak", StatDataType.DYNAMIC, StatTableType.COLUMN, table, MySQLType.INTEGER, "pvpstreak", null), "0");
        table.addStat(addStat("PVPTopStreak", StatDataType.DYNAMIC, StatTableType.COLUMN, table, MySQLType.INTEGER, "pvptopstreak", null), "0");
        table.addStat(addStat("Money", StatDataType.DYNAMIC, StatTableType.COLUMN, table, MySQLType.DOUBLE, "money", null), "0");
        table.addStat(addStat("Trades", StatDataType.INCREASING, StatTableType.COLUMN, table, MySQLType.INTEGER, "trades", null), "0");
    }

    private Stat addStat(String name, StatDataType type, StatTableType tableType, StatsTable table, MySQLType mType, DataLoader loader, DataSaver saver) {
        Stat stat = new DefaultStat(name, type, tableType, table, mType, loader, saver);
        plugin.getStatTypes().put(name, stat);
        return stat;
    }

    private Stat addStat(String name, StatDataType type, StatTableType tableType, StatsTable table, MySQLType mType, String valueColumn, String[] varColumns) {
        Stat stat = new DefaultStat(name, type, tableType, table, mType, valueColumn, varColumns);
        plugin.getStatTypes().put(name, stat);
        return stat;
    }

    private void createBlockTableStats(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "block";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("blockID", MySQLType.INTEGER).addAttributes(NOT_NULL);
        table.addColumn("blockData", MySQLType.BYTE_ARRAY).addAttributes(NOT_NULL);
        table.addColumn("amount", MySQLType.INTEGER).addAttributes(NOT_NULL).setDefault("0");
        table.addColumn("break", MySQLType.BOOLEAN).addAttributes(NOT_NULL);
        table.addStat(addStat("Block break", StatDataType.INCREASING, StatTableType.TABLE, table, MySQLType.INTEGER, new BlockLoader(api), new BlockSaver(api)));
        table.addStat(addStat("Block place", StatDataType.INCREASING, StatTableType.TABLE, table, MySQLType.INTEGER, new BlockLoader(api), new BlockSaver(api)));
    }

    private void createMoveTableStats(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "move";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("type", MySQLType.INTEGER).addAttributes(NOT_NULL);
        table.addColumn("distance", MySQLType.DOUBLE).addAttributes(NOT_NULL).setDefault("0");
        table.addStat(addStat("Move", StatDataType.INCREASING, StatTableType.TABLE, table, MySQLType.DOUBLE, "distance", new String[]{"type"}));
    }

    private void createKillTableStats(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "kill";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("type", MySQLType.STRING).addAttributes(NOT_NULL);
        table.addColumn("amount", MySQLType.INTEGER).addAttributes(NOT_NULL).setDefault("0");
        table.addStat(addStat("Kill", StatDataType.INCREASING, StatTableType.TABLE, table, MySQLType.INTEGER, "amount", new String[]{"type"}));
    }

    private void createDeathTableStats(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "death";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("cause", MySQLType.STRING).addAttributes(NOT_NULL);
        table.addColumn("amount", MySQLType.INTEGER).addAttributes(NOT_NULL).setDefault("0");
        table.addColumn("entity", MySQLType.BOOLEAN).addAttributes(NOT_NULL).setDefault("1");
        table.addStat(addStat("Death", StatDataType.INCREASING, StatTableType.TABLE, table, MySQLType.INTEGER, "amount", new String[]{"cause", "entity"}));
    }

    private void createPlayersTable(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "players";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, false, false));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("player_id", MySQLType.INTEGER).addAttributes(NOT_NULL, PRIMARY_KEY, AUTO_INCREMENT);
        table.addColumn("UUID", MySQLType.STRING);
        table.addColumn("name", MySQLType.STRING);
        table.addStat(api.addStat("Firstjoin", StatDataType.FIXED, StatTableType.COLUMN, table, MySQLType.TIMESTAMP, new FirstjoinLoader(api), new FirstjoinSaver(api)));
    }

    private void createPVPTableStats(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "pvp";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, true, api.isCreatingSnapshots()));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("killed", MySQLType.INTEGER);
        table.addColumn("weapon", MySQLType.STRING);
        table.addColumn("amount", MySQLType.INTEGER);
        table.addStat(addStat("pvp", StatDataType.INCREASING, StatTableType.TABLE, table, MySQLType.DOUBLE, "amount", new String[]{"killed", "weapon"}), "0");
    }

    private void createSystemTable(StatsAPI api) {
        String tableName = api.getDatabasePrefix() + "system";
        if (!api.getStatsTableManager().containsKey(tableName)) {
            api.getStatsTableManager().put(tableName, new StatsTable(tableName, false, false));
        }
        StatsTable table = api.getStatsTable(tableName);
        table.addColumn("version", MySQLType.STRING);
        table.addColumn("timezone", MySQLType.STRING);
    }
}
