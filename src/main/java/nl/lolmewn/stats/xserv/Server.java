package nl.lolmewn.stats.xserv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;

/**
 * @author Sybren
 */
public class Server {

    private final Main plugin;
    private Socket socket;
    private int id;
    private final boolean isHost;
    private Thread keepAliveThread;
    private PrintWriter out;
    private BufferedReader in;

    public Server(Main main, Socket incoming, int id) throws IOException {
        this.plugin = main;
        this.socket = incoming;
        this.id = id;
        this.isHost = true;
        this.setupStreams();
    }

    public Server(Main main, String host, int port) {
        this.plugin = main;
        this.isHost = false;
        try {
            socket = new Socket(host, port);
            socket.setKeepAlive(true);
            this.setupStreams();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setupStreams() throws IOException {
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
    }

    protected void startComms() throws IOException {
        if (isHost) {
            this.keepAliveThread = new Thread(new KeepAlive());
            this.keepAliveThread.start();
            this.sendMessage(Status.HANDSHAKE.getMessage());
            String response;
            if (!(response = this.readMessage()).equals(Status.HANDSHAKE_RESPONSE.getMessage())) {
                plugin.getServer().getLogger().info(
                        String.format("Server tried connecting, handshake failed. Expected %s, but got %s instead.", Status.HANDSHAKE_RESPONSE, response));
                this.socket.close();
            }
        } else {
            String message = this.readMessage();
            if (!message.equals(Status.HANDSHAKE.getMessage())) {
                plugin.getServer().getLogger().info(
                        String.format("Tried connecting to server, handshake failed. Expected %s, but got %s instead.", Status.HANDSHAKE, message));
                this.socket.close();
            }
        }
    }

    private void sendMessage(String message) {
        this.out.println(message);
    }

    private String readMessage() throws IOException {
        return this.in.readLine();
    }

    public void restartSocket() {
    }

    private class KeepAlive implements Runnable {
        //Host pings clients every now and again

        @Override
        public void run() {
            while (socket.isConnected()) {
                try {
                    out.println(Status.PING.getMessage());
                    if (!in.readLine().equals(Status.PONG.getMessage())) {
                        restartSocket();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    Thread.sleep(300 * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

    private class Host implements Runnable {

        @Override
        public void run() {
            while (!socket.isClosed()) {
                try {
                    String receive = readMessage();
                    Status status = Status.getStatus(receive);
                    switch (status) {
                        case REQUEST_SAVE:

                    }
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
