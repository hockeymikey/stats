package nl.lolmewn.stats;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Settings {

    private final Main plugin;
    private String dbHost;
    private String dbPass;
    private String dbUser;
    private String dbName;
    private String dbPrefix;
    private int dbPort;
    private boolean ignoreCreative;
    private boolean debug;
    private boolean sendToGlobal;
    private boolean instaUpdateSigns;
    private int signsUpdateInterval;
    private boolean update;
    private List<String> disabled = new ArrayList<String>();
    private List<String> command = new ArrayList<String>();
    private String snapshotTimeToLive;
    private String snapshotInterval;
    private long previousSnapshot;
    private boolean createSnapshots;
    private boolean isXServerEnabled;
    private boolean isXServerHost;
    private int xServerPort;
    private String xServerHost;
    private boolean usePermissionsForTracking;

    private boolean ignoreAFK;
    private boolean ignoreFlying;

    public boolean isXServerHost() {
        return isXServerHost;
    }

    public boolean isXServerEnabled() {
        return this.isXServerEnabled;
    }

    public int getXServerPort() {
        return xServerPort;
    }

    public String getxServerHost() {
        return xServerHost;
    }

    public boolean isUsingPermissionsForTracking() {
        return this.usePermissionsForTracking;
    }

    public boolean isIgnoreAFK() {
        return ignoreAFK;
    }

    public boolean isIgnoreFlying() {
        return ignoreFlying;
    }

    private Main getPlugin() {
        return this.plugin;
    }

    public Settings(Main m) {
        this.plugin = m;
    }

    public void loadSettings() {
        FileConfiguration f = this.getPlugin().getConfig();
        f.options().copyDefaults(true);
        try {
            f.save(new File(plugin.getDataFolder(), "config.yml"));
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dbUser = f.getString("MySQL-User", "root");
        this.dbPass = f.getString("MySQL-Pass", "root");
        this.dbHost = f.getString("MySQL-Host", "localhost");
        this.dbPort = f.getInt("MySQL-Port", 3306);
        this.dbName = f.getString("MySQL-Database", "minecraft");
        this.dbPrefix = f.getString("MySQL-Prefix", "Stats_");
        this.debug = f.getBoolean("debug", false);
        this.update = f.getBoolean("update", true);
        this.sendToGlobal = f.getBoolean("sendStatsToGlobalServer", true);
        this.ignoreCreative = f.getBoolean("ignoreCreative", false);
        this.ignoreAFK = f.getBoolean("ignoreAFK", false);
        this.ignoreFlying = f.getBoolean("ignoreFlying", false);
        this.instaUpdateSigns = f.getBoolean("signs.insta-update", true);
        this.signsUpdateInterval = f.getInt("signs.update-interval", 10);
        this.snapshotTimeToLive = f.getString("snapshots.timeToLive", "30D");
        this.snapshotInterval = f.getString("snapshots.interval", "1D");
        this.createSnapshots = f.getBoolean("snapshots.enabled", true);
        this.previousSnapshot = f.getLong("snapshots.previous", 0L);
        if (f.contains("disabledStats")) {
            this.disabled = f.getStringList("disabledStats");
        }
        if (f.contains("commandList")) {
            this.command = f.getStringList("commandList");
        }
        this.isXServerEnabled = f.getBoolean("cross-server.enabled", false);
        this.isXServerHost = f.getBoolean("cross-server.isMainServer", false);
        this.xServerHost = f.getString("cross-server.mainServerHost", "localhost");
        this.xServerPort = f.getInt("cross-server.mainServerPort", 25500);
        this.usePermissionsForTracking = f.getBoolean("usePermissionsForTracking", true);
    }

    public void setDebugging(boolean value) {
        this.debug = value;
    }

    public boolean isDebugging() {
        return this.debug;
    }

    public boolean isUpdating() {
        return update;
    }

    public String getDbHost() {
        return dbHost;
    }

    public String getDbName() {
        return dbName;
    }

    public String getDbPass() {
        return dbPass;
    }

    public int getDbPort() {
        return dbPort;
    }

    public String getDbPrefix() {
        return dbPrefix;
    }

    public String getDbUser() {
        return dbUser;
    }

    public boolean isSendToGlobal() {
        return sendToGlobal;
    }

    public boolean isIgnoreCreative() {
        return ignoreCreative;
    }

    public boolean isInstaUpdateSigns() {
        return instaUpdateSigns;
    }

    public int getSignsUpdateInterval() {
        return signsUpdateInterval;
    }

    public List<String> getDisabledStats() {
        return this.disabled;
    }

    public List<String> getCommand() {
        return command;
    }

    public String getSnapshotTTL() {
        return this.snapshotTimeToLive;
    }

    public String getSnapshotInterval() {
        return this.snapshotInterval;
    }

    public boolean createSnapshots() {
        return this.createSnapshots;
    }

    public long getPreviousSnapshot() {
        return this.previousSnapshot;
    }
}
